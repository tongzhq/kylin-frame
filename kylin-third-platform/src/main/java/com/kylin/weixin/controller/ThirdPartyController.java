package com.kylin.weixin.controller;

import com.kylin.utils.FileUtils;
import com.kylin.weixin.utils.WeChat3rdManageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:   第三方平台controller
 * <p>
 * Created by tongzhq on 2020/6/2 0002 14:32:17
 *
 * @author tongzhq
 * @version 1.0
 */
@RestController
@RequestMapping("/thirdParty")
public class ThirdPartyController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @RequestMapping("/authEvent")
    public void authEvent(HttpServletRequest request, HttpServletResponse response) {
        LOGGER.info(">>>>>>>>>>>>>>>>>>>>>>>>开始接收微信推送凭据信息<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

        WeChat3rdManageUtil.decodeMessage(request);
        try {
            response.getWriter().write("success");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
