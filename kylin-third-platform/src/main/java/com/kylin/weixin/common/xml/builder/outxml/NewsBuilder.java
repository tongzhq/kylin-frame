package com.kylin.weixin.common.xml.builder.outxml;


import com.kylin.weixin.common.xml.bean.WeChatXmlOutNewsMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 图文消息builder
 *
 * @author chanjarster
 */
public final class NewsBuilder extends BaseBuilder<NewsBuilder, WeChatXmlOutNewsMessage> {
  private List<WeChatXmlOutNewsMessage.Item> articles = new ArrayList<>();

  public NewsBuilder addArticle(WeChatXmlOutNewsMessage.Item... items) {
    Collections.addAll(this.articles, items);
    return this;
  }

  public NewsBuilder articles(List<WeChatXmlOutNewsMessage.Item> articles){
    this.articles = articles;
    return this;
  }

  @Override
  public WeChatXmlOutNewsMessage build() {
    WeChatXmlOutNewsMessage m = new WeChatXmlOutNewsMessage();
    for (WeChatXmlOutNewsMessage.Item item : this.articles) {
      m.addArticle(item);
    }
    setCommon(m);
    return m;
  }

}
