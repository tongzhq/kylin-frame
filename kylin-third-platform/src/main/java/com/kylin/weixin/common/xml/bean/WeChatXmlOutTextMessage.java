package com.kylin.weixin.common.xml.bean;

import com.kylin.weixin.common.WeChatConstant;
import com.kylin.weixin.common.xml.utils.XStreamCDataConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * 文本类消息
 */
@XStreamAlias("xml")
public class WeChatXmlOutTextMessage extends WeChatXmlOutMessage {

  private static final long serialVersionUID = -3668626558982039453L;

  @XStreamAlias("Content")
  @XStreamConverter(value = XStreamCDataConverter.class)
  private String content;

  public WeChatXmlOutTextMessage() {
    this.msgType = WeChatConstant.XmlMsgType.TEXT;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
