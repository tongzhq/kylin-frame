package com.kylin.weixin.common.xml.builder.outxml;


import com.kylin.weixin.common.xml.bean.WeChatXmlOutTextMessage;

/**
 * 文本消息builder
 *
 * @author chanjarster
 */
public final class TextBuilder extends BaseBuilder<TextBuilder, WeChatXmlOutTextMessage> {
  private String content;

  public TextBuilder content(String content) {
    this.content = content;
    return this;
  }

  @Override
  public WeChatXmlOutTextMessage build() {
    WeChatXmlOutTextMessage m = new WeChatXmlOutTextMessage();
    setCommon(m);
    m.setContent(this.content);
    return m;
  }
}
