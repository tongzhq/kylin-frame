package com.kylin.weixin.common.json.builder;


import com.kylin.weixin.common.WeChatConstant;
import com.kylin.weixin.common.json.kefu.kfmessage.WeChatMpKfMessage;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description: 图文消息builder
 * <p>
 * Created by tongzhq on 2019/12/21 0021 10:40
 *
 * @author tongzhq
 * @version 1.0
 */
public class MpNewsBuilder extends BaseBuilder<MpNewsBuilder> {

    private String mediaId;

    public MpNewsBuilder() {
        this.msgType = WeChatConstant.KefuMsgType.MPNEWS;
    }

    public MpNewsBuilder mediaId(String mediaId) {
        this.mediaId = mediaId;
        return this;
    }

    @Override
    public WeChatMpKfMessage build() {
        WeChatMpKfMessage m = super.build();
        m.setMpNewsMediaId(this.mediaId);
        return m;
    }
}