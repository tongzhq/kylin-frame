package com.kylin.weixin.common.xml.builder.outxml;


import com.kylin.weixin.common.xml.bean.WeChatXmlOutImageMessage;

/**
 * 图片消息builder
 *
 * @author chanjarster
 */
public final class ImageBuilder extends BaseBuilder<ImageBuilder, WeChatXmlOutImageMessage> {

  private String mediaId;

  public ImageBuilder mediaId(String media_id) {
    this.mediaId = media_id;
    return this;
  }

  @Override
  public WeChatXmlOutImageMessage build() {
    WeChatXmlOutImageMessage m = new WeChatXmlOutImageMessage();
    setCommon(m);
    m.setMediaId(this.mediaId);
    return m;
  }

}
