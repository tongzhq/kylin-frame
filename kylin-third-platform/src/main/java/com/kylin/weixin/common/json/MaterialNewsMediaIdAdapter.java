package com.kylin.weixin.common.json;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:  给图文素材对象中的mediaID 追加一个type前缀 image
 * <p>
 * Created by tongzhq on 2019/12/18 0018 14:05
 *
 * @author tongzhq
 * @version 1.0
 */
public class MaterialNewsMediaIdAdapter extends TypeAdapter {
    @Override
    public void write(JsonWriter jsonWriter, Object o) throws IOException {

        if (o == null) {
            jsonWriter.nullValue();
        } else {
            String str = o.toString();
            str = str.split(":")[1];
            jsonWriter.value(str);
        }
    }

    @Override
    public Object read(JsonReader jsonReader) throws IOException {
        return "image:" + jsonReader.nextString();
    }
}