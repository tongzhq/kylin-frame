package com.kylin.weixin.common.json.builder;


import com.kylin.weixin.common.WeChatConstant;
import com.kylin.weixin.common.json.kefu.kfmessage.WeChatMpKfMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2019/12/21 0021 10:43
 *
 * @author tongzhq
 * @version 1.0
 */
public class NewsBuilder extends BaseBuilder<NewsBuilder> {

    private List<WeChatMpKfMessage.WxArticle> articles = new ArrayList<>();

    public NewsBuilder() {
        this.msgType = WeChatConstant.KefuMsgType.NEWS;
    }

    public NewsBuilder addArticle(WeChatMpKfMessage.WxArticle... articles) {
        Collections.addAll(this.articles, articles);
        return this;
    }

    public NewsBuilder articles(List<WeChatMpKfMessage.WxArticle> articles) {
        this.articles = articles;
        return this;
    }

    @Override
    public WeChatMpKfMessage build() {
        WeChatMpKfMessage m = super.build();
        m.setArticles(this.articles);
        return m;
    }
}