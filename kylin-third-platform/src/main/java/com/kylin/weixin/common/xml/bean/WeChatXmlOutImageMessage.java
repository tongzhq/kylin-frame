package com.kylin.weixin.common.xml.bean;

import com.kylin.weixin.common.WeChatConstant;
import com.kylin.weixin.common.xml.utils.XStreamMediaIdConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description: 图片消息
 * <p>
 * Created by tongzhq on 2019/12/10 0010 9:58
 *
 * @author tongzhq
 * @version 1.0
 */

@XStreamAlias("xml")
public class WeChatXmlOutImageMessage extends WeChatXmlOutMessage {

    private static final long serialVersionUID = 7989057958586883025L;
    @XStreamAlias("Image")
    @XStreamConverter(value = XStreamMediaIdConverter.class)
    private String mediaId;

    public WeChatXmlOutImageMessage() {
        this.msgType = WeChatConstant.XmlMsgType.IMAGE;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}