package com.kylin.weixin.common.json.builder;


import com.kylin.weixin.common.WeChatConstant;
import com.kylin.weixin.common.json.kefu.kfmessage.WeChatMpKfMessage;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description: 文本消息builder
 * <p>
 * Created by tongzhq on 2019/12/21 0021 10:36
 *
 * @author tongzhq
 * @version 1.0
 */
public final class TextBuilder extends BaseBuilder<TextBuilder> {

    private String content;

    public TextBuilder() {
        this.msgType = WeChatConstant.KefuMsgType.TEXT;
    }

    public TextBuilder content(String content) {
        this.content = content;
        return this;
    }

    @Override
    public WeChatMpKfMessage build() {
        WeChatMpKfMessage m = super.build();
        m.setContent(this.content);
        return m;
    }
}