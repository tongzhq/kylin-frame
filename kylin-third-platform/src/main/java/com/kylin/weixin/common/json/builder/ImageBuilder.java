package com.kylin.weixin.common.json.builder;


import com.kylin.weixin.common.WeChatConstant;
import com.kylin.weixin.common.json.kefu.kfmessage.WeChatMpKfMessage;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description: 图片消息builder
 * <p>
 * Created by tongzhq on 2019/12/21 0021 10:39
 *
 * @author tongzhq
 * @version 1.0
 */
public class ImageBuilder extends BaseBuilder<ImageBuilder> {
    private String mediaId;

    public ImageBuilder() {
        this.msgType = WeChatConstant.KefuMsgType.IMAGE;
    }

    public ImageBuilder mediaId(String media_id) {
        this.mediaId = media_id;
        return this;
    }

    @Override
    public WeChatMpKfMessage build() {
        WeChatMpKfMessage m = super.build();
        m.setMediaId(this.mediaId);
        return m;
    }
}