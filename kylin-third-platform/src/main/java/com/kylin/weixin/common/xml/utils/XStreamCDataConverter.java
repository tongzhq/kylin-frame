package com.kylin.weixin.common.xml.utils;

import com.thoughtworks.xstream.converters.basic.StringConverter;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description: XML内容加上CDATA标签
 * <p>
 * Created by tongzhq on 2019/12/9 0009 20:08
 *
 * @author tongzhq
 * @version 1.0
 */
public class XStreamCDataConverter extends StringConverter {

  @Override
  public String toString(Object obj) {
    return "<![CDATA[" + super.toString(obj) + "]]>";
  }

}
