package com.kylin.weixin.common.json.kefu.kfmessage;

import com.google.gson.GsonBuilder;
import com.kylin.weixin.common.json.WeChatMpKfMessageGsonAdapter;
import com.kylin.weixin.common.json.builder.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2019/12/21 0021 10:32
 *
 * @author tongzhq
 * @version 1.0
 */
public class WeChatMpKfMessage implements Serializable {

    private static final long serialVersionUID = 6846100676187235078L;
    private String toUser;
    private String msgType;
    private String content;
    private String mediaId;
    private String thumbMediaId;
    private String title;
    private String description;
    private String musicUrl;
    private String hqMusicUrl;
    private String kfAccount;
    private String cardId;
    private String mpNewsMediaId;
    private String miniProgramAppId;
    private String miniProgramPagePath;
    private String headContent;
    private String tailContent;
    private List<WxArticle> articles = new ArrayList<>();

    /**
     * 菜单消息里的菜单内容.
     */
    private List<MsgMenu> msgMenus = new ArrayList<>();

    /**
     * 获得文本消息builder.
     */
    public static TextBuilder TEXT() {
        return new TextBuilder();
    }

    /**
     * 获得图片消息builder.
     */
    public static ImageBuilder IMAGE() {
        return new ImageBuilder();
    }


    /**
     * 获得图文消息（点击跳转到外链）builder.
     */
    public static NewsBuilder NEWS() {
        return new NewsBuilder();
    }

    /**
     * 获得图文消息（点击跳转到图文消息页面）builder.
     */
    public static MpNewsBuilder MPNEWS() {
        return new MpNewsBuilder();
    }


    /**
     * 获得菜单消息builder.
     */
    public static WxMsgMenuBuilder MSGMENU() {
        return new WxMsgMenuBuilder();
    }

    /**
     * 小程序卡片.
     */
    public static MiniProgramPageBuilder MINIPROGRAMPAGE() {
        return new MiniProgramPageBuilder();
    }

    public String toJson() {
        return new GsonBuilder().setPrettyPrinting().disableHtmlEscaping()
                .registerTypeAdapter(WeChatMpKfMessage.class, new WeChatMpKfMessageGsonAdapter())
                .create().toJson(this);
    }

    public static class WxArticle implements Serializable {
        private static final long serialVersionUID = 5145137235440507379L;

        private String title;
        private String description;
        private String url;
        private String picUrl;

        public WxArticle() {
        }

        public WxArticle(String title, String description, String url, String picUrl) {
            this.title = title;
            this.description = description;
            this.url = url;
            this.picUrl = picUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }
    }


    public static class MsgMenu implements Serializable {
        private static final long serialVersionUID = 7020769047598378839L;

        private String id;
        private String content;

        public MsgMenu() {
        }

        public MsgMenu(String id, String content) {
            this.id = id;
            this.content = content;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public void setMsgType(String msgType) {
        this.msgType = msgType;
    }

    public String getMsgType() {
        return msgType;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getThumbMediaId() {
        return thumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId) {
        this.thumbMediaId = thumbMediaId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMusicUrl() {
        return musicUrl;
    }

    public void setMusicUrl(String musicUrl) {
        this.musicUrl = musicUrl;
    }

    public String getHqMusicUrl() {
        return hqMusicUrl;
    }

    public void setHqMusicUrl(String hqMusicUrl) {
        this.hqMusicUrl = hqMusicUrl;
    }

    public String getKfAccount() {
        return kfAccount;
    }

    public void setKfAccount(String kfAccount) {
        this.kfAccount = kfAccount;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getMpNewsMediaId() {
        return mpNewsMediaId;
    }

    public void setMpNewsMediaId(String mpNewsMediaId) {
        this.mpNewsMediaId = mpNewsMediaId;
    }

    public String getMiniProgramAppId() {
        return miniProgramAppId;
    }

    public void setMiniProgramAppId(String miniProgramAppId) {
        this.miniProgramAppId = miniProgramAppId;
    }

    public String getMiniProgramPagePath() {
        return miniProgramPagePath;
    }

    public void setMiniProgramPagePath(String miniProgramPagePath) {
        this.miniProgramPagePath = miniProgramPagePath;
    }

    public String getHeadContent() {
        return headContent;
    }

    public void setHeadContent(String headContent) {
        this.headContent = headContent;
    }

    public String getTailContent() {
        return tailContent;
    }

    public void setTailContent(String tailContent) {
        this.tailContent = tailContent;
    }

    public List<WxArticle> getArticles() {
        return articles;
    }

    public void setArticles(List<WxArticle> articles) {
        this.articles = articles;
    }

    public List<MsgMenu> getMsgMenus() {
        return msgMenus;
    }

    public void setMsgMenus(List<MsgMenu> msgMenus) {
        this.msgMenus = msgMenus;
    }
}