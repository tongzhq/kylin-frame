package com.kylin.weixin.common.json.builder;


import com.kylin.weixin.common.json.kefu.kfmessage.WeChatMpKfMessage;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2019/12/21 0021 10:14
 *
 * @author tongzhq
 * @version 1.0
 */
public class BaseBuilder<T> {

    protected String msgType;
    protected String toUser;

    @SuppressWarnings("unchecked")
    public T toUser(String toUser) {
        this.toUser = toUser;
        return (T) this;
    }

    public WeChatMpKfMessage build() {
        WeChatMpKfMessage m = new WeChatMpKfMessage();
        m.setMsgType(this.msgType);
        m.setToUser(this.toUser);
        return m;
    }
}