package com.kylin.weixin.common.json.builder;


import com.kylin.weixin.common.WeChatConstant;
import com.kylin.weixin.common.json.kefu.kfmessage.WeChatMpKfMessage;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description: 小程序卡片
 * <p>
 * Created by tongzhq on 2019/12/21 0021 10:47
 *
 * @author tongzhq
 * @version 1.0
 */
public class MiniProgramPageBuilder extends BaseBuilder<MiniProgramPageBuilder> {

    private String title;
    private String appId;
    private String pagePath;
    private String thumbMediaId;

    public MiniProgramPageBuilder() {
        this.msgType = WeChatConstant.KefuMsgType.MINIPROGRAMPAGE;
    }


    public MiniProgramPageBuilder title(String title) {
        this.title = title;
        return this;
    }

    public MiniProgramPageBuilder appId(String appId) {
        this.appId = appId;
        return this;
    }


    public MiniProgramPageBuilder pagePath(String pagePath) {
        this.pagePath = pagePath;
        return this;
    }


    public MiniProgramPageBuilder thumbMediaId(String thumbMediaId) {
        this.thumbMediaId = thumbMediaId;
        return this;
    }


    @Override
    public WeChatMpKfMessage build() {
        WeChatMpKfMessage m = super.build();
        m.setTitle(this.title);
        m.setMiniProgramAppId(this.appId);
        m.setMiniProgramPagePath(this.pagePath);
        m.setThumbMediaId(this.thumbMediaId);
        return m;
    }
}