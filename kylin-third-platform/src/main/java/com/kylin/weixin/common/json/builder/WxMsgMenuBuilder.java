package com.kylin.weixin.common.json.builder;


import com.kylin.weixin.common.WeChatConstant;
import com.kylin.weixin.common.json.kefu.kfmessage.WeChatMpKfMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description: 菜单消息
 * <p>
 * Created by tongzhq on 2019/12/21 0021 10:45
 *
 * @author tongzhq
 * @version 1.0
 */
public class WxMsgMenuBuilder extends BaseBuilder<WxMsgMenuBuilder> {

    private List<WeChatMpKfMessage.MsgMenu> msgMenus = new ArrayList<>();
    private String headContent;
    private String tailContent;


    public WxMsgMenuBuilder() {
        this.msgType = WeChatConstant.KefuMsgType.MSGMENU;
    }

    public WxMsgMenuBuilder addMenus(WeChatMpKfMessage.MsgMenu... msgMenus) {
        Collections.addAll(this.msgMenus, msgMenus);
        return this;
    }

    public WxMsgMenuBuilder msgMenus(List<WeChatMpKfMessage.MsgMenu> msgMenus) {
        this.msgMenus = msgMenus;
        return this;
    }

    public WxMsgMenuBuilder headContent(String headContent) {
        this.headContent = headContent;
        return this;
    }

    public WxMsgMenuBuilder tailContent(String tailContent) {
        this.tailContent = tailContent;
        return this;
    }

    @Override
    public WeChatMpKfMessage build() {
        WeChatMpKfMessage m = super.build();
        m.setHeadContent(this.headContent);
        m.setTailContent(this.tailContent);
        m.setMsgMenus(this.msgMenus);
        return m;
    }
}