package com.kylin.weixin.config;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Program Name:
 * <p>
 * Description: 第三方平台配置信息
 * <p>
 * Created by tongzhq on 2020/6/2 0002 15:25:42
 *
 * @author tongzhq
 * @version 1.0
 */
@EqualsAndHashCode
@Data
public class ThirdPartyConfig {
    /**
     * 第三方平台AppID
     */
    private String wx3rdAppId;

    /**
     * 第三方平台AppSecret
     */
    private String wx3rdAppSecret;

    /**
     * 第三方平台消息校验Token--微信开放平台->大家平台——自测
     */
    private String wx3rdToken;

    /**
     * 第三方平台消息加解密Key--微信开放平台->大家平台——自测
     */
    private String wx3rdEncodingAesKey;

}
