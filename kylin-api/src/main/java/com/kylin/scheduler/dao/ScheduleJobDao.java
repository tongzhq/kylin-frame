package com.kylin.scheduler.dao;

import com.kylin.base.dao.BaseDao;
import com.kylin.scheduler.entity.ScheduleJob;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description:
 * @author: kylin
 * @create: 2018-02-06 11:31
 **/
@Mapper
public interface ScheduleJobDao extends BaseDao<ScheduleJob> {
}
