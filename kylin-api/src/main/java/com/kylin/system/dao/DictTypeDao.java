package com.kylin.system.dao;

import com.kylin.base.dao.BaseDao;
import com.kylin.system.entity.DictType;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description:
 * @author: kylin
 * @create: 2018-02-01 10:14
 **/
@Mapper
public interface DictTypeDao extends BaseDao<DictType> {

}