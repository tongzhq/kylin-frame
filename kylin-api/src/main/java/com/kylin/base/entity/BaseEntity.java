package com.kylin.base.entity;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:   基础实体基类
 * <p>
 * Created by tongzhq on 2020/6/2 0002 18:20:33
 *
 * @author tongzhq
 * @version 1.0
 */

@Data
public abstract class BaseEntity implements Serializable {
    private static final long serialVersionUID = -2141757056169918465L;

    private Timestamp createTime;

    private Timestamp modifyTime;
}
