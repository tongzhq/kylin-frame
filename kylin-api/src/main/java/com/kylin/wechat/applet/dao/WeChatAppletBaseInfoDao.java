package com.kylin.wechat.applet.dao;

import com.kylin.base.dao.BaseDao;
import com.kylin.wechat.applet.entity.WeChatAppletBaseInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2020/6/2 0002 18:40:22
 *
 * @author tongzhq
 * @version 1.0
 */
@Mapper
public interface WeChatAppletBaseInfoDao extends BaseDao<WeChatAppletBaseInfo> {
}
