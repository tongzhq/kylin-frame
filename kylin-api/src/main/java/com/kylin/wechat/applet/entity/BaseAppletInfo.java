package com.kylin.wechat.applet.entity;

import com.kylin.base.entity.BaseEntity;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:   小程序基础信息基类
 * <p>
 * Created by tongzhq on 2020/6/2 0002 18:27:14
 *
 * @author tongzhq
 * @version 1.0
 */
public abstract class BaseAppletInfo extends BaseEntity {
    private static final long serialVersionUID = -9091571057726461846L;

    /**
     * 小程序appID
     */
    private String appId;

    /**
     * 晓小程序secret
     */
    private String appletSecret;

    /**
     * 小程序名称
     */
    private String appletName;

    /**
     * 头像
     */
    private String avatar;
}
