package com.kylin.wechat.applet.entity;

/**
 * Program Name: dajia-wxapplet
 * <p>
 * Description:   微信小程序基础信息
 * <p>
 * Created by tongzhq on 2020/6/2 0002 18:19:51
 *
 * @author tongzhq
 * @version 1.0
 */
public class WeChatAppletBaseInfo extends BaseAppletInfo {
    private static final long serialVersionUID = 1392544690744883939L;

    /**
     * 主键id
     */
    private String id;
}
