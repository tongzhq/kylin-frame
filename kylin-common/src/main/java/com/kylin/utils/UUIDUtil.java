package com.kylin.utils;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.util.UUID;

/**
 * Program Name: kylin-frame
 * <p>
 * Description:
 * <p>
 * Created by tongzhq on 2019/6/11 10:44
 *
 * @author tongzhq
 * @version 1.0
 */
public class UUIDUtil {
    public UUIDUtil() {
    }

    public static String getUUID() {
        return String.valueOf(getLongUUID());
    }

    public static Long getLongUUID() {
        long val = -1L;

        do {
            UUID uid = UUID.randomUUID();
            ByteBuffer buffer = ByteBuffer.wrap(new byte[16]);
            buffer.putLong(uid.getLeastSignificantBits());
            buffer.putLong(uid.getMostSignificantBits());
            BigInteger bi = new BigInteger(buffer.array());
            val = bi.longValue();
        } while(val < 0L);

        return val;
    }
}